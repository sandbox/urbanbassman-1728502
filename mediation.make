; Drupal version
core = 7.x
api = 2

projects[wysiwyg][subdir] = contrib
; Use dev release until the features integration is released with 2.2.
projects[wysiwyg][version] = 2.x-dev
; Ckeditor
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][directory_name] = "ckeditor"
; WYMeditor
libraries[wymeditor][download][type] = "get"
libraries[wymeditor][download][url] = "https://github.com/downloads/wymeditor/wymeditor/wymeditor-1.0.0b3.tar.gz"
libraries[wymeditor][directory_name] = "wymeditor"

; tinymce
libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://github.com/downloads/tinymce/tinymce/tinymce_3.5.6.zip"
libraries[tinymce][directory_name] = "tinymce"

projects[wysiwyg_template][subdir] = contrib
projects[wysiwyg_template][version] = 2.7

projects[imce][subdir] = contrib
projects[imce][version] = 1.5

projects[imce_wysiwyg][subdir] = contrib
projects[imce_wysiwyg][version] = 1.0

; workflow

projects[workflow][subdir] = contrib
projects[workflow][version] = 1.0

projects[rules][subdir] = contrib
projects[rules][version] = 2.2

; yui
libraries[yui][download][type] = "get"
libraries[yui][download][url] = "http://developer.yahoo.com/yui/download/"
libraries[yui][directory_name] = "yui"

projects[features][subdir] = contrib
projects[features][version] = 1.0-beta2

projects[field_permissions][subdir] = contrib
projects[field_permissions][version] = 1.0-rc2

projects[advanced_help][subdir] = contrib
projects[advanced_help][version] = 1.0

projects[captcha][subdir] = contrib
projects[captcha][version] = 1.0-beta2
projects[captcha][patch][http://drupal.org/files/issues/825088-19-captcha_ctools_export.patch] = http://drupal.org/files/issues/825088-19-captcha_ctools_export.patch

projects[context][subdir] = contrib
projects[context][version] = 3.0-beta2

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.1

projects[date][subdir] = contrib
projects[date][version] = 2.5

projects[ds][subdir] = contrib
projects[ds][version] = 1.5

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.0

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0-rc1

projects[styles][subdir] = contrib
projects[styles][version] = 2.0-alpha8

projects[token][subdir] = contrib
projects[token][version] = 1.4

projects[views][subdir] = contrib
projects[views][version] = 3.3

;CIT documentation

projects[cck_blocks][type] = "module"
projects[cck_blocks][subdir] = "custom"
projects[cck_blocks][version] = "1.1"

projects[ds][type] = "module"
projects[ds][subdir] = "custom"
projects[ds][version] = "1.5"

projects[entity][type] = "module"
projects[entity][subdir] = "custom"
projects[entity][version] = "1.0-rc2"

projects[feeds][type] = "module"
projects[feeds][subdir] = "custom"
projects[feeds][version] = "2.0-alpha4"

projects[feeds_tamper][type] = "module"
projects[feeds_tamper][subdir] = "custom"
projects[feeds_tamper][version] = "1.0-beta3"

projects[feeds_xpathparser][type] = "module"
projects[feeds_xpathparser][subdir] = "custom"
projects[feeds_xpathparser][version] = "1.0-beta3"

projects[job_scheduler][type] = "module"
projects[job_scheduler][subdir] = "custom"
projects[job_scheduler][version] = "2.0-alpha2"

projects[search_by_page][type] = "module"
projects[search_by_page][subdir] = "custom"
projects[search_by_page][version] = "1.2"

projects[superfish][type] = "module"
projects[superfish][subdir] = "custom"
projects[superfish][version] = "1.8"

projects[lightbox2][type] = "module"
projects[lightbox2][subdir] = "custom"
projects[lightbox2][version] = "1.0-beta1"

;projects[nodequeue][type] = "module"
;projects[nodequeue][subdir] = "custom"
;projects[nodequeue][version] = "2.0-beta1"

projects[syntaxhighlighter][type] = "module"
projects[syntaxhighlighter][subdir] = "custom"
projects[syntaxhighlighter][version] = "1.1"

projects[views_data_export][type] = "module"
projects[views_data_export][subdir] = "custom"
projects[views_data_export][version] = "3.0-beta5"

; Libraries
libraries[superfish][download][type] = "get"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal"
libraries[superfish][directory_name] = "superfish"

libraries[syntaxhighlighter][download][type] = "get"
libraries[syntaxhighlighter][download][url] = "https://github.com/alexgorbatchev/SyntaxHighlighter"
libraries[syntaxhighlighter][directory_name] = "syntaxhighlighter"

;custom

projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "custom"
projects[backup_migrate][version] = "2.2"

;projects[better_formats][type] = "module"
;projects[better_formats][subdir] = "custom"
;projects[better_formats][version] = "1.0-beta1"

projects[draggableviews][type] = "module"
projects[draggableviews][subdir] = "custom"
projects[draggableviews][version] = "2.0-beta1"

projects[flag][type] = "module"
projects[flag][subdir] = "custom"
projects[flag][version] = "2.0-beta6"

projects[flag_weights][type] = "module"
projects[flag_weights][subdir] = "custom"
projects[flag_weights][version] = "1.x-dev"

projects[features_extra][type] = "module"
projects[features_extra][subdir] = "custom"
projects[features_extra][version] = "1.x-dev"

projects[node_export][type] = "module"
projects[node_export][subdir] = "custom"
projects[node_export][version] = "3.0-rc2"

projects[uuid][type] = "module"
;projects[uuid][subdir] = "custom"
;projects[uuid][version] = "1.0-alpha3"
projects[uuid][download][type] = "get"
projects[uuid][download][url] = "http://ftp.drupal.org/files/projects/uuid-7.x-1.x-dev.tar.gz"

projects[views_data_export][type] = "module"
projects[views_data_export][subdir] = "custom"
projects[views_data_export][version] = "3.0-beta6"

projects[views_flag_refresh][type] = "module"
projects[views_flag_refresh][subdir] = "custom"
projects[views_flag_refresh][version] = "1.0-beta1"

projects[attic][type] = "module"
projects[attic][download][type] = "git"
projects[attic][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1651712.git"
projects[attic][subdir] = "custom"

projects[big_dipper][type] = "module"
projects[big_dipper][download][type] = "git"
projects[big_dipper][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1616440.git"
projects[big_dipper][subdir] = "custom"

projects[cit_content_types][type] = "module"
projects[cit_content_types][download][type] = "git"
projects[cit_content_types][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1516256.git"
projects[cit_content_types][subdir] = "custom"

;projects[cit_menus][type] = "module"
;projects[cit_menus][download][type] = "git"
;projects[cit_menus][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1616956.git"
;projects[cit_menus][subdir] = "custom"

projects[cit_views][type] = "module"
projects[cit_views][download][type] = "git"
projects[cit_views][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1516284.git"
projects[cit_views][subdir] = "custom"

projects[cit_importers][type] = "module"
projects[cit_importers][download][type] = "git"
projects[cit_importers][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1516230.git"
projects[cit_importers][subdir] = "custom"

projects[cit_display][type] = "module"
projects[cit_display][download][type] = "git"
projects[cit_display][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1526282.git"
projects[cit_display][subdir] = "custom"

projects[cit_permissions][type] = "module"
projects[cit_permissions][download][type] = "git"
projects[cit_permissions][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1617922.git"
projects[cit_permissions][subdir] = "custom"

projects[mediation_roles][type] = "module"
projects[mediation_roles][download][type] = "git"
projects[mediation_roles][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1728512.git"
projects[mediation_roles][subdir] = "custom"

projects[mediation_editor][type] = "module"
projects[mediation_editor][download][type] = "git"
projects[mediation_editor][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1740960.git"
projects[mediation_editor][subdir] = "custom"

projects[mediation_workflows][type] = "module"
projects[mediation_workflows][download][type] = "git"
projects[mediation_workflows][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1738268.git"
projects[mediation_workflows][subdir] = "custom"

;projects[mind_games_deltas][type] = "module"
;projects[mind_games_deltas][download][type] = "git"
;projects[mind_games_deltas][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1548536.git"
;projects[mind_games_deltas][subdir] = "custom"

;projects[mind_games_xhtml_deltas][type] = "module"
;projects[mind_games_xhtml_deltas][download][type] = "git"
;projects[mind_games_xhtml_deltas][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1576382.git"
;projects[mind_games_xhtml_deltas][subdir] = "custom"

projects[mediation_xhtml_deltas][type] = "module"
projects[mediation_xhtml_deltas][download][type] = "git"
projects[mediation_xhtml_deltas][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1756218.git"
projects[mediation_xhtml_deltas][subdir] = "custom"

;theme
projects[omega][type] = "module"
projects[omega][subdir] = "custom"

projects[zen][type] = "module"
projects[zen][subdir] = "custom"

projects[navin][type] = "module"
projects[navin][subdir] = "custom"

projects[alphorn][type] = "module"
projects[alphorn][subdir] = "custom"

projects[conch][type] = "module"
projects[conch][subdir] = "custom"

projects[respond][type] = "module"
projects[respond][subdir] = "custom"

projects[tej][type] = "module"
projects[tej][subdir] = "custom"

projects[ingrained][type] = "module"
projects[ingrained][subdir] = "custom"


projects[lner][type] = "theme"
projects[lner][download][type] = "git"
projects[lner][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1493136.git"
projects[lner][subdir] = "custom"

projects[mind_games][type] = "theme"
projects[mind_games][download][type] = "git"
projects[mind_games][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1510666.git"
projects[mind_games][subdir] = "custom"

projects[mind_games_xhtml][type] = "theme"
projects[mind_games_xhtml][download][type] = "git"
projects[mind_games_xhtml][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1575778.git"
projects[mind_games_xhtml][subdir] = "custom"

projects[mediation_xhtml][type] = "theme"
projects[mediation_xhtml][download][type] = "git"
projects[mediation_xhtml][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1754630.git"
projects[mediation_xhtml][subdir] = "custom"

projects[cit][type] = "theme"
projects[cit][download][type] = "git"
projects[cit][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1509432.git"
projects[cit][subdir] = "custom"

;omega supporting modules
projects[omega_tools][type] = "module"
projects[omega_tools][subdir] = "custom"
projects[delta][type] = "module"
projects[delta][subdir] = "custom"

;libraries
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta1.tar.gz"
